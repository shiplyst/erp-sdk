<?php

/**
 * @Author: Deepanshu Srivastava
 * @Date: 2019-10-22 19:30:46
 * @Desc:
 */

namespace ERP;

use ERP\Base;
use Exception;

class BlDocument extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->httpClient->setResource('jobs');
    }

    public function getBlDetails($params)
    {
        try {
            $this->validateData($params);
            return $this->httpClient->get('get-bl-details', $params);
        } catch (Exception $e) {
            return [
                'success' => false,
                'data' => [],
                'error_code' => $e->getMessage(),
            ];
        }
    }
}
