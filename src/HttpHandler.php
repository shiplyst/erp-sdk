<?php
namespace ERP;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class HttpHandler
{
    private $base_uri;
    private $resource;
    private $headers;
    private $end_point;
    private $httpClient;
    private $response_body;

    const HTTP_GET = "GET";
    const HTTP_POST = "POST";
    const HTTP_PUT = "PUT";

    const HTTP_ERRORS = false;
    const CONNECT_TIMEOUT = 1000;

    public function __construct()
    {
        $this->httpClient = new Client([
            'base_uri' => $this->base_uri,
        ]);
    }

    public function setBaseUri(string $value = null)
    {
        $this->base_uri = $value;
    }

    public function getBaseUri()
    {
        return $this->base_uri;
    }

    public function setResource(string $value = null)
    {
        $this->resource = $value;
    }

    public function getResource()
    {
        return $this->resource;
    }

    public function setHeaders(array $values)
    {
        $this->headers = $values;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function setEndPoint(string $uri_segments = null)
    {
        $this->end_point = "{$this->getBaseUri()}/{$this->getResource()}";
        if (!empty($uri_segments)) {
            $this->end_point .= "/$uri_segments";
        }
        return $this->end_point;
    }

    public function getEndPoint()
    {
        return $this->end_point;
    }

    private function getFormattedResponseBody()
    {
        return json_decode($this->response_body->__toString(), true);
    }

    public function get(string $uri_segments, array $params = [])
    {
        try {
            $this->setEndPoint($uri_segments);

            $response = $this->httpClient->request(
                self::HTTP_GET,
                $this->getEndPoint(),
                [
                    'headers' => $this->getHeaders(),
                    'query' => $params,
                    'http_errors' => self::HTTP_ERRORS,
                    'connect_timeout' => self::CONNECT_TIMEOUT,
                ]
            );
            $this->response_body = $response->getBody();
            return $this->getFormattedResponseBody();
        } catch (RequestException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function post($uri_segments, $params)
    {
        try {
            $this->setEndPoint($uri_segments);

            $response = $this->httpClient->request(
                self::HTTP_POST,
                $this->getEndPoint(),
                [
                    'headers' => $this->getHeaders(),
                    'json' => $params,
                    'http_errors' => self::HTTP_ERRORS,
                    'connect_timeout' => self::CONNECT_TIMEOUT,
                ]
            );
            $this->response_body = $response->getBody();
            return $this->getFormattedResponseBody();
        } catch (RequestException $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
