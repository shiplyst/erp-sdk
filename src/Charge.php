<?php
/**
 * @Author: Mohammad Samsuddin
 * @Date: 2019-10-22 14:54:23
 * @Desc:
 */

namespace ERP;

use ERP\Base;
use Exception;

class Charge extends Base
{

    public function __construct()
    {
        parent::__construct();
        $this->httpClient->setResource('charges');
    }

    public function search($params)
    {
        try {
            $this->validateData($params);
            return $this->httpClient->get('search', $params);
        } catch (Exception $e) {
            return [
                'success' => false,
                'data' => [],
                'error_code' => $e->getMessage(),
            ];
        }
    }

    public function getAll()
    {
        try {
            $this->hasErpApiKeyUrl();
            $this->setErpHeaders();
            return $this->httpClient->get('list');
        } catch (Exception $e) {
            return [
                'success' => false,
                'data' => [],
                'error_code' => $e->getMessage(),
            ];
        }
    }

    public function validateData($data)
    {
        $this->hasErpApiKeyUrl();
        $this->hasData($data);

        $this->setErpHeaders();
    }

    public function createSeaExport($params)
    {
        try {
            $this->hasErpApiKeyUrl();
            $this->hasData($params);
            $this->setErpHeaders();
            return $this->httpClient->post('create', $params);
        } catch (Exception $e) {
            return [
                'success' => false,
                'data' => [],
                'error_code' => $e->getMessage(),
            ];
        }
    }

}
