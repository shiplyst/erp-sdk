<?php

/**
 * @Author: Deepanshu Srivastava
 * @Date: 2019-10-22 19:30:46
 * @Desc:
 */

namespace ERP;

use ERP\Base;
use Exception;

class Invoice extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->httpClient->setResource('jobs');
    }

    public function getInvoices($params)
    {
        try {
            $this->validateData($params);
            return $this->httpClient->get('get-invoices', $params);
        } catch (Exception $e) {
            return [
                'success' => false,
                'data' => [],
                'error_code' => $e->getMessage(),
            ];
        }
    }
}
