<?php
/**
 * @Author: Deepanshu Srivastava
 * @Date: 2019-10-22 19:38:27
 * @Desc:
 */

namespace ERP;

use ERP\Base;
use Exception;

class CutOffDates extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->httpClient->setResource('jobs');
    }

    public function getCutOffDetails($params)
    {
        try {
            $this->validateData($params);
            return $this->httpClient->get('get-cut-off-details', $params);
        } catch (Exception $e) {
            return [
                'success' => false,
                'data' => [],
                'error_code' => $e->getMessage(),
            ];
        }
    }
}
