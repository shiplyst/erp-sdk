<?php
namespace ERP;

use ERP\BlDocument;
use ERP\Charge;
use ERP\ContainerDetails;
use ERP\CutOffDates;
use ERP\Invoice;
use ERP\Job;
use ERP\Partner;
use Exception;

class WpErp
{
    public static function getFactoryObject($type)
    {
        if (!isset($type)) {
            throw new Exception("Object Type not set");
        }
        if (empty($type)) {
            throw new Exception("Object Type cannot be empty");
        }
        $objName = '';
        switch ($type) {
            case 'job':
                $objName = new Job();
                break;
            case 'bl-document':
                $objName = new BlDocument();
                break;
            case 'invoice':
                $objName = new Invoice();
                break;
            case 'container-details':
                $objName = new ContainerDetails();
                break;
            case 'cut-off-dates':
                $objName = new CutOffDates();
                break;
            case 'partner':
                $objName = new Partner();
                break;
            case 'charge':
                $objName = new Charge();
                break;
            case 'users':
                $objName = new Users();
                break;
            case 'sea-carriers':
                $objName = new SeaCarriers();
                break;
            default:
                $objName = '';
                break;
        }
        if (empty($objName)) {
            throw new Exception("Invalid Object Type");
        }
        return $objName;
    }
}
