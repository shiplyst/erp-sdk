<?php

/**
 * @Author: Deepanshu Srivastava
 * @Date: 2019-10-22 13:38:39
 * @Desc:
 */

namespace ERP;

use Exception;

class Base
{
    protected $erp_url;
    protected $api_endpoints;
    protected $erp_api_key;
    protected $httpClient;

    const HEADER_API_KEY = 'Api-Key';

    public function __construct()
    {
        $this->httpClient = new \ERP\HttpHandler();
    }

    public function setErpUrl($erp_url)
    {
        $this->erp_url = $erp_url;
        $this->httpClient->setBaseUri($this->getErpUrl() . "/services");
    }

    public function getErpUrl()
    {
        return $this->erp_url;
    }

    public function getErpApiKey()
    {
        return $this->erp_api_key;
    }

    public function setErpApiKey($erp_api_key)
    {
        $this->erp_api_key = $erp_api_key;
    }

    protected function setErpHeaders()
    {
        $this->httpClient->setHeaders(
            [
                self::HEADER_API_KEY => $this->getErpApiKey(),
            ]
        );
    }

    protected function hasErpApiKeyUrl()
    {
        if (empty($this->erp_api_key) || empty($this->erp_url)) {
            throw new Exception("Invalid API Key or Url");
        }
        return true;
    }

    protected function hasData($data)
    {
        if (empty($data) || !is_array($data)) {
            throw new Exception("Invalid Parameters");
        }
        return true;
    }

    protected function hasJobNumber($data)
    {
        if (empty($data['job_number'])) {
            throw new Exception("Shipment is not linked with ERP Job");
        }
        return true;
    }

    public function validateData($data)
    {
        $this->hasErpApiKeyUrl();
        $this->hasData($data);
        $this->hasJobNumber($data);

        $this->setErpHeaders();
    }
}
