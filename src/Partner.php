<?php
/**
 * @Author: Mohammad Samsuddin
 * @Date: 2019-10-22 14:54:23
 * @Desc:
 */

namespace ERP;

use ERP\Base;
use Exception;

class Partner extends Base
{

    public function __construct()
    {
        parent::__construct();
        $this->httpClient->setResource('partners');
    }

    public function search($params)
    {
        try {
            $this->validateData($params);
            return $this->httpClient->get('search', $params);
        } catch (Exception $e) {
            return [
                'success' => false,
                'data' => [],
                'error_code' => $e->getMessage(),
            ];
        }
    }

    public function getAll()
    {
        try {
            $this->hasErpApiKeyUrl();
            $this->setErpHeaders();
            return $this->httpClient->get('list');
        } catch (Exception $e) {
            return [
                'success' => false,
                'data' => [],
                'error_code' => $e->getMessage(),
            ];
        }
    }

    public function validateData($data)
    {
        $this->hasErpApiKeyUrl();
        $this->hasData($data);

        $this->setErpHeaders();
    }

}
