<?php
/*
 * @Author: Deepanshu Srivastava
 * @Date: 2021-01-22 18:31:27
 * @Last Modified by: Deepanshu Srivastava
 * @Last Modified time: 2021-01-22 18:32:55
 */

namespace ERP;

use ERP\Base;
use Exception;

class Users extends Base
{

    public function __construct()
    {
        parent::__construct();
        $this->httpClient->setResource('users');
    }

    public function search($params)
    {
        try {
            $this->validateData($params);
            return $this->httpClient->get('search', $params);
        } catch (Exception $e) {
            return [
                'success' => false,
                'data' => [],
                'error_code' => $e->getMessage(),
            ];
        }
    }

    public function getAll()
    {
        try {
            $this->hasErpApiKeyUrl();
            $this->setErpHeaders();
            return $this->httpClient->get('list');
        } catch (Exception $e) {
            return [
                'success' => false,
                'data' => [],
                'error_code' => $e->getMessage(),
            ];
        }
    }

    public function validateData($data)
    {
        $this->hasErpApiKeyUrl();
        $this->hasData($data);

        $this->setErpHeaders();
    }

}
